#ifndef HOMADEUS_SPI_DRIVER_H
#define HOMADEUS_SPI_DRIVER_H

#include <stdint.h>

/* TODO: Move to AVR Architecture */
#include <avr/io.h>

#define SPI_CLOCK_DIV4 0x00
#define SPI_CLOCK_DIV16 0x01
#define SPI_CLOCK_DIV64 0x02
#define SPI_CLOCK_DIV128 0x03
#define SPI_CLOCK_DIV2 0x04
#define SPI_CLOCK_DIV8 0x05
#define SPI_CLOCK_DIV32 0x06
//#define SPI_CLOCK_DIV64 0x07

#define SPI_MODE0 0x00
#define SPI_MODE1 0x04
#define SPI_MODE2 0x08
#define SPI_MODE3 0x0C

#define SPI_MODE_MASK 0x0C  // CPOL = bit 3, CPHA = bit 2 on SPCR
#define SPI_CLOCK_MASK 0x03  // SPR1 = bit 1, SPR0 = bit 0 on SPCR
#define SPI_2XCLOCK_MASK 0x01  // SPI2X = bit 0 on SPSR

/* SPI transfer flags */
/* Assert CS before transfer */
#define SPI_XFER_BEGIN	0x01
/* Deassert CS after transfer */
#define SPI_XFER_END	0x02

#define DIR_SPI_PORT DDRB
#define MOSI_PIN     PB2
#define SCK_PIN      PB1

typedef struct spi_slave {
    volatile uint8_t*  cs_port;
    volatile uint8_t*  cs_port_dir;
    uint8_t   cs_pin;
} spi_slave_t;

#if defined(SPI_HAVE_CLAIM_BUS)
int spi_claim_bus(struct spi_slave *slave);
void spi_release_bus(struct spi_slave *slave);
#endif

int  spi_xfer(struct spi_slave *slave, unsigned int bitlen, const void *dout,
        void *din, unsigned long flags);
int spi_arch_assert_cs(struct spi_slave *slave, uint8_t status);
int spi_arch_init_spi(struct spi_slave *slave);
int spi_arch_fini_spi(struct spi_slave *slave);

#endif /* end of include guard: HOMADEUS_SPI_DRIVER_H */
