#include <stdlib.h>
#include <stdio.h>

#include "spi/spi.h"

#include <homadeus/utils/utils.h>

#if defined(SPI_HAVE_CLAIM_BUS)
static uint8_t claimed = 0;

int spi_claim_bus(struct spi_slave *slave)
{
	if (claimed == 1) {
		return 1;
	}

	/* Setup architecture specific spi slave */
	spi_arch_init_spi(slave);

	/* Claim the bus for our needs */
	claimed = 1;

	return 0;
}

void spi_release_bus(struct spi_slave *slave)
{
	/* Just in case CS was left asserted */
	spi_arch_assert_cs(slave, 0);

	spi_arch_fini_spi(slave);

	/* Claim the bus for our needs */
	claimed = 0;
}
#endif

int  spi_xfer(struct spi_slave *slave, unsigned int bitlen, const void *dout,
		void *din, unsigned long flags) {

	unsigned int len = bitlen / 8;

	/* Assert the cs_pin */
	if (flags & SPI_XFER_BEGIN) {
#if !defined(SPI_HAVE_CLAIM_BUS)
		spi_arch_init_spi(slave);
#endif
		spi_arch_assert_cs(slave, 1);
	}

	uint8_t *in = din;
	const uint8_t *out = dout;

	for(unsigned int i = 0; i < len; i++) {

		uint8_t data_ = 0x00;
		if (dout != NULL) {
			data_ = *out++;
		} else {
			data_ = 0x00;
		}

		SPDR = data_;
		while (!(SPSR & _BV(SPIF)))
			;
		data_ = SPDR;

		if (din != NULL) {
			*in++ = data_;
		}
	}

	/* Deassert the cs_pin */
	if (flags & SPI_XFER_END) {
		spi_arch_assert_cs(slave, 0);
#if !defined(SPI_HAVE_CLAIM_BUS)
		spi_arch_fini_spi(slave);
#endif
	}

	return 0;
}

int spi_arch_assert_cs(struct spi_slave *slave, uint8_t status)
{
	if (status) {
		*(slave->cs_port) &= ~(_BV(slave->cs_pin));
	} else {
		*(slave->cs_port) |= _BV(slave->cs_pin);
	}
	*(slave->cs_port_dir) |= _BV(slave->cs_pin);

	return 0;
}

int spi_arch_init_spi(struct spi_slave *slave)
{
	/* Ensure CS is not selected */
	spi_arch_assert_cs(slave, 0);

	SPCR |= _BV(MSTR);
	SPCR |= _BV(SPE);

	DIR_SPI_PORT |=
		   _BV(SCK_PIN) |
		   _BV(MOSI_PIN);

	return 0;
}

int spi_arch_fini_spi(__attribute__((unused)) struct spi_slave *slave)
{
	SPCR &= ~_BV(SPE);

	return 0;
}

